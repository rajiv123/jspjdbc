<%@ page import="com.itglance.jspjdbc.dao.CustomerDAO" %>
<%@ page import="com.itglance.jspjdbc.dao.impl.CustomerDAOImpl" %>
<%@ page import="com.itglance.jspjdbc.entity.Customer" %><%--
  Created by IntelliJ IDEA.
  User: rajiv
  Date: 7/13/2018
  Time: 10:00 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <title>Customer</title>
</head>
<body class="container">

    <%
        if(request.getMethod().equalsIgnoreCase("POST")){
            CustomerDAO customerDAO = new CustomerDAOImpl();
            Customer customer = new Customer();
            customer.setFirstName(request.getParameter("firstname"));
            customer.setLastName(request.getParameter("lastname"));
            customer.setAge(Integer.parseInt(request.getParameter("age")));
            try {
                if (customerDAO.insert(customer) > 0) {
                    response.sendRedirect("index.jsp");
                }
            }catch(Exception e) {
                out.println(e.getMessage());
            }
        }
    %>

    <h1>Add Customer</h1>
    <hr/>
    <form method="post">
        <div class="form-group">
            <label for="firstname">First Name</label>
            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name">
        </div>

        <div class="form-group">
            <label for="lastname">Last Name</label>
            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name">
        </div>

        <div class="form-group">
            <label for="age">Age</label>
            <input type="number" class="form-control" id="age" name="age" placeholder="Age">
        </div>

        <input type="submit" class="btn btn-success" value="Save"/>
        <a href="index.jsp" class="btn btn-danger">Back</a>
    </form>
</body>
</html>
