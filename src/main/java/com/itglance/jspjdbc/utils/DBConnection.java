package com.itglance.jspjdbc.utils;

import java.sql.*;

public class DBConnection {

    private Connection conn;
    private PreparedStatement stmt;

    public DBConnection() {

    }

    public void open() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection("jdbc:mysql://localhost/itglance", "root", null);
    }

    public PreparedStatement initStatement(String sql) throws SQLException {
        stmt = conn.prepareStatement(sql);
        return stmt;
    }

    public ResultSet executeQuery() throws SQLException {
        return stmt.executeQuery();
    }

    public int executeUpdate() throws SQLException {
        return stmt.executeUpdate();
    }

    public void close() throws SQLException {
        if(conn != null && !conn.isClosed()) {
            conn.close();
            conn = null;
        }
    }
}
