package com.itglance.jspjdbc.dao.impl;

import com.itglance.jspjdbc.dao.CustomerDAO;
import com.itglance.jspjdbc.entity.Customer;
import com.itglance.jspjdbc.utils.DBConnection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomerDAOImpl implements CustomerDAO {

    DBConnection connection = new DBConnection();

    public List<Customer> getAll() throws ClassNotFoundException, SQLException {
        List<Customer> customerList = new ArrayList<Customer>();
        connection.open();
        String sql = "SELECT * FROM customers";
        connection.initStatement(sql);
        ResultSet rs = connection.executeQuery();
        while(rs.next()) {
            Customer customer = new Customer();
            customer.setId(rs.getInt("id"));
            customer.setFirstName(rs.getString("first_name"));
            customer.setLastName(rs.getString("last_name"));
            customer.setAge(rs.getInt("age"));
            customerList.add(customer);

        }
        connection.close();
        return customerList;
    }

    public int insert(Customer customer) throws ClassNotFoundException, SQLException {
        connection.open();
        String sql = "INSERT INTO customers(first_name, last_name, age) value(?, ?, ?);";
        PreparedStatement stmt = connection.initStatement(sql);
        stmt.setString(1, customer.getFirstName());
        stmt.setString(2, customer.getLastName());
        stmt.setInt(3, customer.getAge());
        int result = stmt.executeUpdate();
        connection.close();
        return result;
    }

}
