package com.itglance.jspjdbc.dao;

import com.itglance.jspjdbc.entity.Customer;

import java.sql.SQLException;
import java.util.List;

public interface CustomerDAO {

    List<Customer> getAll() throws ClassNotFoundException, SQLException;
    int insert(Customer customer) throws ClassNotFoundException, SQLException;

}
