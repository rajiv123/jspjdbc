<%@ page import="com.itglance.jspjdbc.dao.CustomerDAO" %>
<%@ page import="com.itglance.jspjdbc.dao.impl.CustomerDAOImpl" %>
<%@ page import="com.itglance.jspjdbc.entity.Customer" %>
<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: rajiv
  Date: 7/13/2018
  Time: 10:00 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <title>Customer</title>
</head>
<body class="container">

    <div class="pull-right">
        <p>
            <a href="addcourse.jsp" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>Add</a>
        </p>
    </div>
<% try {%>
<table class="table table-striped">
    <thead>
    <tr>
        <th>Id</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Age</th>
        <th>Action</th>
    </tr>
    </thead>

<%
            CustomerDAO customerDAO = new CustomerDAOImpl();

            for(Customer customer: customerDAO.getAll()) {

        %>
    <tr>
        <td><%=customer.getId()%></td>
        <td><%=customer.getFirstName()%></td>
        <td><%=customer.getLastName()%></td>
        <td><%=customer.getAge()%></td>
        <td>
            <a href="#" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> </a>
            <a href="#" class="btn btn-danger" onclick="return confirm('Are you sure to delete this record?')"><span class="glyphicon glyphicon-trash"></span></a>
        </td>
    </tr>

        <% } %>
        </table>
        <%
        }catch(ClassNotFoundException e) {
            out.println(e.getMessage());
        }catch (SQLException e) {
            out.println(e.getMessage());
        }
    %>
</body>
</html>
